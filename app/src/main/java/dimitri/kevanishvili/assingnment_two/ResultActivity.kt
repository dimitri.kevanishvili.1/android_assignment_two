package dimitri.kevanishvili.assingnment_two

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_activity)
        val actionbar = supportActionBar
        actionbar!!.title = "Result"
        actionbar.setDisplayHomeAsUpEnabled(true)
        val result = intent.getFloatExtra(Consts.INTENT_RESULT_KEY, 0f)
        val resultTextView = findViewById<TextView>(R.id.tv_result)
        resultTextView.text = result.toString();
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}