package dimitri.kevanishvili.assingnment_two

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionbar = supportActionBar
        actionbar!!.title = "Calc"

        val numA = findViewById<EditText>(R.id.et_A)
        val numB = findViewById<EditText>(R.id.et_B)
        val btnSum = findViewById<Button>(R.id.btn_sum)
        val btnSub = findViewById<Button>(R.id.btn_sub)
        val btnMult = findViewById<Button>(R.id.btn_mult)
        val btnDiv = findViewById<Button>(R.id.btn_div)
        btnSum.setOnClickListener {
            openResultActivity(
                sum(
                    getFloatValueFromEditText(numA), getFloatValueFromEditText(
                        numB
                    )
                )
            )
        }
        btnSub.setOnClickListener {
            openResultActivity(
                subtract(
                    getFloatValueFromEditText(numA),
                    getFloatValueFromEditText(numB)
                )
            )
        }
        btnMult.setOnClickListener {
            openResultActivity(
                multiply(
                    getFloatValueFromEditText(numA),
                    getFloatValueFromEditText(numB)
                )
            )
        }
        btnDiv.setOnClickListener {
            openResultActivity(
                divide(
                    getFloatValueFromEditText(numA),
                    getFloatValueFromEditText(numB)
                )
            )
        }
    }

    private fun openResultActivity(result: Float) {
        print(result)
        val intent = Intent(this, ResultActivity::class.java)
        intent.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT)
        intent.putExtra(Consts.INTENT_RESULT_KEY, result)
        startActivity(intent);
    }

    private fun sum(a: Float, b: Float): Float {
        return a + b;
    }

    private fun subtract(a: Float, b: Float): Float {
        return a - b;
    }

    private fun divide(a: Float, b: Float): Float {
        return a / b;
    }

    private fun multiply(a: Float, b: Float): Float {
        return a * b;
    }

    private fun getFloatValueFromEditText(editText: EditText): Float {
        return try {
            val value = editText.text.toString().toFloat();
            value;
        } catch (e: NumberFormatException) {
            0f;
        }
    }
}